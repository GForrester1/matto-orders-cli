import Vue from 'vue'
import Router from 'vue-router'
// import firebase from 'firebase'
import NewOrders from './views/NewOrders.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    // {
    //   path: '/',
    //   name: 'home',
    //   component: NewOrders
    // },
    {
      path: '/new-orders',
      name: 'NewOrders',
      component: NewOrders
    },
    {
      path: '/current-orders',
      name: 'CurrentOrders',
      component: () => import('./views/CurrentOrders.vue')
    },
    {
      path: '/complete-orders',
      name: 'CompleteOrders',
      component: () => import('./views/CompleteOrders.vue')
    },
    {
      path: '*',
      redirect: '/new-orders'
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
