import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import Header from './components/Header.vue'
import fb from './firebaseConfig.js'
import utilities from './utilities/utility';

Vue.component('header-view', Header);
Vue.mixin( utilities.mixin());

Vue.config.productionTip = false;


let app;
fb.auth.onAuthStateChanged(user => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App),
      created(){
        // this.$store.dispatch('fetchOrders');
      }
    }).$mount('#app')
  }
})
