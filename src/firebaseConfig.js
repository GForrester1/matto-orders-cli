import firebase from 'firebase'
import 'firebase/firestore'

// firebase init goes here
// const config = {
//     apiKey: "AIzaSyD67NNcu9BGo51XGqGvOyVxfcJ2JUFS1io",
//     authDomain: "matto-dev.firebaseapp.com",
//     databaseURL: "https://matto-dev.firebaseio.com",
//     projectId: "matto-dev",
//     storageBucket: "matto-dev.appspot.com",
//     messagingSenderId: "1046459271258"
// }
const config = {
    apiKey: "AIzaSyDhnVItZL3rSiyqEwzpgKUr-DEzmFoVh6o",
    authDomain: "matto-staging.firebaseapp.com",
    databaseURL: "https://matto-staging.firebaseio.com",
    projectId: "matto-staging",
    storageBucket: "matto-staging.appspot.com",
    messagingSenderId: "7575402600"
}
firebase.initializeApp(config)

// firebase utils
const db = firebase.firestore()
const auth = firebase.auth()
const currentUser = auth.currentUser

// date issue fix according to firebase
const settings = {
    
}
db.settings(settings)

// firebase collections
const ordersCollection = db.collection('order');

export default {
    db,
    auth,
    currentUser,
    ordersCollection
}