import Vue from 'vue'
import Vuex from 'vuex'
import fb from './firebaseConfig.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    newOrders: [],
    currentOrders: [],
    completeOrders: []
  },
  mutations: {
    setOrders(state,val){
      state[`${val.status}Orders`] = val.docs;
    }
  },
  actions: {
    fetchOrders({commit,state}){
      fb.ordersCollection.where('status','==','NEW').onSnapshot(snapshot => {
      // fb.ordersCollection.onSnapshot(snapshot => {
        commit('setOrders',{status:'new',docs: snapshot.docs})
      });
      fb.ordersCollection.where('status','==','IN_PROGRESS').onSnapshot(snapshot => {
        commit('setOrders',{status:'current',docs: snapshot.docs})
      });
      fb.ordersCollection.where('status','==','READY').onSnapshot(snapshot => {
        commit('setOrders',{status:'complete',docs: snapshot.docs})
      });
    }
  }
})
